# README #

Vagrant with Ansible for Magento 2 project.

### Set up ###

* Open group_vars/all.yml and configure it according to your preferences
* Edit hosts file (on your host machine)

**repo.magento.com credentials**

If your host has `~/.composer/auth.json` with repo.magento.com credentials than they will be copied and used. Alternatively credentials can be configured using `group_vars/all.yml` at following node:

```
    auth_json:
      username:
      password:

```

**Sample data**

VM is configured to install sample data for you. If you do not wish to install sample data open `group_vars/all.yml` and change following:

```
    sample_data: True

```

To

```
    sample_data: False

```

Once sample data is installed `sample_data` config option is changed automatically you:

```
    sample_data: Installed

```

### Prerequisites ###

* Vagrant v => 2.0.2
* VirtualBox
* Guest Additions
* Ansible v => 2.4.3.0